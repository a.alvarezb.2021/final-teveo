# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

* Nombre: Alejandro Álvarez Bustos
* Titulación: Tecnologías de la telecomunucación
* Cuenta en laboratorios: aalvarez
* Cuenta URJC: a.alvarezb.2021
* Video básico (url): https://www.youtube.com/watch?v=yomW1XT4GcM
* Video parte opcional (url): https://www.youtube.com/watch?v=uFZjXQ735aQ
* Despliegue (url): https://alejadroalvarez.pythonanywhere.com/
* Contraseñas: admin
* Cuenta Admin Site: admin/admin

## Resumen parte obligatoria
* Cuando se intenta acceder a otro recurso que no es /, antes de pasar primero por /, se redirecciona a /, para
* mandar una cookie de sesión.

## Lista partes opcionales

* Nombre parte: paginación página principal main.
* Nombre parte: paginación página de las cámaras.
* Nombre parte: incluir favicon.
* Nombre parte: permitir que las sesiones se cierren, se resetea su configuración.
* Nombre parte: permitir votar las cámaras mediante un botón de like.
* Nombre parte: incluir test unitarios invocando mas de un recurso, y pycodestyle.
* Nombre parte: Obtención de camaras de dos nuevas fuentes de datos, estas en formato JSON.


## WHITELIST

* No me descargan imagenes en pythonanywhere, mientras que si lo hago desde mi ordenador si funciona.
* Algunas imagenes no cargarn en python pythonanywhere, mientras que si lo hago desde mi ordenador si carga.

