from django.apps import AppConfig


class TeveoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'TeVeO'
