from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from .models import Camara

class CameraHandler(ContentHandler):

    def __init__(self):
        self.inCamera = False
        self.inContent = False
        self.content = ""
        self.id = ""
        self.src = ""
        self.longitud = ""
        self.latitud= ""
        self.lugar = ""

    def startElement(self, name, attrs):
        if name == "camara":
            self.inCamera = True

        elif name == "cam":
            self.inCamera = True
            id_lista2 = "LIST2-"
            self.id = attrs.getValue("id")
            self.id = id_lista2 + self.id
        elif self.inCamera:
            if name == "url":
                self.inContent = True
            elif name == "info":
                self.inContent = True
            elif name == "latitude":
                self.inContent = True
            elif name == "longitude":
                self.inContent = True

    def endElement(self, name):
        if name == "camara" or name == "cam":
            self.inCamera = False
            c = Camara(
                id=self.id,
                source=self.src,
                lugar=self.lugar,
                longitud=self.longitud,
                latitud=self.latitud
            )
            c.save()
        elif self.inCamera:
            if name == "url":
                self.src = self.content
                self.inContent = False
                self.content = ""
            elif name == "info":
                self.lugar = self.content
                self.inContent = False
                self.content = ""
            elif name == "latitude":
                self.latitud += self.content
                self.inContent = False
                self.content = ""
            elif name == "longitude":
                self.longitud += self.content
                self.inContent = False
                self.content = ""

    def characters(self, chars):
        if self.inContent:
            self.content = self.content + chars

class CameraInfo2:

    def __init__(self, xml):
        self.parse = make_parser()
        self.handler = CameraHandler()
        self.parse.setContentHandler(self.handler)
        self.parse.parse(xml)