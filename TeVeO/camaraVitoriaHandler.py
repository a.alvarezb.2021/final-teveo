import urllib.request
import json
import ssl
from .models import Camara

def download_cameras(url):
    try:
        context = ssl._create_unverified_context()
        # Realizar la solicitud y obtener la respuesta
        with urllib.request.urlopen(url, context=context) as response:
            # Si es 200 Ok seguimos
            if response.getcode() == 200:
                # Leer el contenido de la respuesta
                response_data = response.read().decode("utf-8")
                # Intentar cargar el contenido como JSON
                data = json.loads(response_data)

                # verdadera informacion
                true_data = data["features"]
                #creamos las camaras
                for feature in true_data:
                    id_JSON = "Vitoria-"
                    c = Camara(id=id_JSON + feature["id"], source=feature["properties"]["imagen"],
                               lugar=feature["properties"]["nombre"],
                               longitud=feature["geometry"]["coordinates"][0],
                               latitud=feature["geometry"]["coordinates"][1])
                    c.save()

            else:
                print("Error al obtener los datos:", response.getcode())
    except Exception as e:
        print("Error:", e)


