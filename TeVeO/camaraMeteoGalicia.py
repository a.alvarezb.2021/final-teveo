import urllib.request
import json
from .models import Camara


def download_galicia(url):
    try:
        with urllib.request.urlopen(url) as response:
            # Si es 200 Ok seguimos
            if response.getcode() == 200:
                # Leer el contenido de la respuesta
                response_data = response.read().decode("utf-8")
                # Intentar cargar el contenido como JSON
                data = json.loads(response_data)
                # verdadera informacion
                camaras_data = data["listaCamaras"]

                for camara in camaras_data:
                    id_galicia = "MeteoGalicia-"
                    c = Camara(id=id_galicia + str(camara["identificador"]), source=camara["imaxeCamara"],
                               lugar=camara["nomeCamara"], longitud=str(camara["lon"]),
                               latitud=str(camara["lat"]))
                    c.save()
            else:
                print("Error para encontrar los datos")
    except Exception as e:
        print("Error:", e)

