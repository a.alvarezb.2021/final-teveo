from django.contrib import admin
from django.contrib.sessions.models import Session
from .models import Camara, Comentario, Configuracion, Listas, LikeCamara

# Register your models here.
admin.site.register(Comentario)
admin.site.register(Camara)
admin.site.register(Configuracion)
admin.site.register(Listas)
admin.site.register(LikeCamara)
admin.site.register(Session)
