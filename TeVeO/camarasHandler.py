from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from .models import Camara


class CameraHandler(ContentHandler):

        def __init__(self):
            self.inCamera = False
            self.inContent = False
            self.content = ""
            self.id = ""
            self.src = ""
            self.coordenadas = ""
            self.lugar = ""
            self.longitud = ""
            self.latitud = ""

        def startElement(self, name, attrs):
            if name == "camara":
                self.inCamera = True
            elif self.inCamera:
                if name == "lugar":
                    self.inContent = True
                elif name == "src":
                    self.inContent = True
                elif name == "coordenadas":
                    self.inContent = True
                elif name == "id":
                    self.inContent = True

        def endElement(self, name):
            if name == "camara":
                self.inCamera = False
                c = Camara(
                    id=self.id,
                    source=self.src,
                    lugar=self.lugar,
                    longitud=self.longitud,
                    latitud=self.latitud,
                )
                c.save()
            elif self.inCamera:
                if name == "src":
                    self.src = self.content
                    self.inContent = False
                    self.content = ""
                elif name == "id":
                    id_lista1 = "LIST1-"
                    self.id = self.content
                    self.id = id_lista1 + self.id
                    self.inContent = False
                    self.content = ""
                elif name == "lugar":
                    self.lugar = self.content
                    self.inContent = False
                    self.content = ""
                elif name == "coordenadas":
                    self.coordenadas = self.content
                    cortinades_split = self.content.split(",")
                    self.longitud = cortinades_split[1]
                    self.latitud = cortinades_split[0]
                    self.inContent = False
                    self.content = ""

        def characters(self, chars):
            if self.inContent:
                self.content = self.content + chars

class CameraInfo:

    def __init__(self, xml):
        self.parse = make_parser()
        self.handler = CameraHandler()
        self.parse.setContentHandler(self.handler)
        self.parse.parse(xml)