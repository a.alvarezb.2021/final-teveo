from django.shortcuts import render,  redirect
from django.http import HttpResponse, Http404
from django.utils import timezone
from django.urls import reverse
import base64
import random
from .camarasHandler import CameraInfo
from .camarasL2Handler import CameraInfo2
from .camaraVitoriaHandler import download_cameras
from .camaraMeteoGalicia import download_galicia
from .models import Camara, Comentario, Configuracion, Listas, LikeCamara
import urllib.request
import urllib.error
import urllib.parse
import json
import uuid
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage


def main(request):
    """
    Se encargar de devolver la pagina principal,
    donde se ve una serie de comentarios puestos
    por los visitantes

    :param request:
    :return: pagina principal de la app
    """
    try:
        # Obtener los comentarios ordenados de más reciente a más antiguo
        comentarios = Comentario.objects.all().order_by("-fecha")
        # Obtener todas las cámaras
        camaras = Camara.objects.all()
        # Obtener el ID de sesión del navegador actual
        session_id = request.session.get("session_id")
        confi = Configuracion.objects.filter(sessionid=session_id).exists()
        if not session_id or not confi:
            # Si no hay un ID de sesión, o no existe una
            # configuración para este ID de sesión,
            # crear uno nuevo y guardar la configuración
            # predeterminada para esta sesión
            session_id = str(uuid.uuid4())
            request.session["session_id"] = session_id
            configuracion = (Configuracion
                             (sessionid=session_id,
                              nombre="Anónimo",
                              size="medium",
                              tipo="serif"))
            configuracion.save()
        else:
            # Obtener la configuración específica de esta sesión
            configuracion = Configuracion.objects.get(sessionid=session_id)
        # Muestra 10 comentarios por página
        paginador = Paginator(comentarios, 3)
        numero_pagina = request.GET.get("page")
        try:
            pagina_comentarios = paginador.page(numero_pagina)
        except PageNotAnInteger:
            pagina_comentarios = paginador.page(1)
        except EmptyPage:
            pagina_comentarios = paginador.page(paginador.num_pages)

        # Devolver la página con la configuración y otros datos necesarios
        return render(request, "TeVeO/main.html",
                      {"comentarios": pagina_comentarios,
                       "camaras": camaras,
                       "configuracion": configuracion})
    except (Comentario.DoesNotExist, Camara.DoesNotExist):
        return render(request, "TeVeO/main.html", {})


def descargarLista4():
    """
    Funcion encargada de descargar la lista de camaras
    de las lista de Meteo Galicia

    """
    url = ("https://servizos.meteogalicia.gal"
           "/mgrss/observacion/jsonCamaras.action")

    download_galicia(url)


def descargarLista3():
    """
    Funcion encargada de descargar la lista de camaras
    de las lista de Meteo Galicia

    """
    url = ("https://www.vitoria-gasteiz.org"
           "/c11-01w/cameras?action=list&format=GEOJSON")

    download_cameras(url)


def descargarLista2():
    """
    Funcion encargada de descargar el contenido relacionado con la lista2

    """
    url = ("https://gitlab.eif.urjc.es/cursosweb"
           "/2023-2024/final-teveo/-/raw/main/listado2.xml")
    xml = urllib.request.urlopen(url)
    CameraInfo2(xml)


def descargarLista1():
    """
    Funcion encargada de descargar el contenido relacionado con la lista1

    """
    url = ("https://gitlab.eif.urjc.es/cursosweb"
           "/2023-2024/final-teveo/-/raw/main/listado1.xml")
    xml = urllib.request.urlopen(url)
    CameraInfo(xml)


def order_num_coments(camaras):
    """
    Funcion que nos devuelve las camaras,
    ordenes segun el que tenga mayor comentarios
    """
    cams = []

    for i in camaras:
        longitu_coment = len(Comentario.objects.filter(camera=i))
        cams.append((i, longitu_coment))

    for i in range(len(cams)):
        for j in range(i+1, len(cams)):
            if cams[i][1] > cams[j][1]:
                cams[i], cams[j] = cams[j], cams[i]

    camaras_ordernas = []
    for cam, _ in reversed(cams):
        camaras_ordernas.append(cam)

    return camaras_ordernas


def jsonCamara(request):

    id = request.GET.get("id")

    camara = Camara.objects.get(id=id)

    info = {
        "id": camara.id,
        "source": camara.source,
        "lugar": camara.lugar,
        "latitud": camara.latitud,
        "longitud": camara.longitud,
        "num_comentarios":  len(Comentario.objects.filter(camera=id)),
        "likes": len(LikeCamara.objects.filter(camara=id)),
    }

    return HttpResponse(json.dumps(info), content_type="application/json")


def camaras(request):
    """
    Vista encargada de , si llega un POST,
    esto quiere decir que se decarga una de las listas,
    decarga el contenido de las lista y lo guarda en la base de datos
    Si le llega un GET,
    devuelve la pagina de las camaras con las camaras descargadas

    :param request:
    :return: redireciona siempre a la pagina de las camras
    """
    if request.method == "POST":
        # Crea un objeto lista o devuelve el que se crea,
        # no se le añade ningun parametro mas porque solo quiero uno
        Lista, _ = Listas.objects.get_or_create()
        # Si en el POST se encuentra en name lista1,
        # y el valor del modelo es not False=True
        if "lista1" in request.POST and not Lista.lista_1:
            # Cambio el valor del booleano del modelo
            Lista.lista_1 = True
            Lista.save()
            # Descargo el contenido de la lista1
            descargarLista1()

        # Si en el POST se encuentra en name lista2,
        # y el valor del modelo es not False=True
        elif "lista2" in request.POST and not Lista.lista_2:
            # Cambio el valor del booleano del modelo
            Lista.lista_2 = True
            Lista.save()
            # Descargo el contenido de la lista2
            descargarLista2()

        elif "lista3" in request.POST and not Lista.lista_3:
            # Cambio el valor del booleano del modelo
            Lista.lista_3 = True
            Lista.save()
            # Descargo el contenido de la lista3
            descargarLista3()

        elif "lista4" in request.POST and not Lista.lista_4:
            # Cambio el valor del booleano del modelo
            Lista.lista_4 = True
            Lista.save()
            # Descargo el contenido de la lista4
            descargarLista4()
        # Redireciono a la pagina de las camars
        return redirect("camaras")

    elif request.method == "GET":
        try:
            camaras = Camara.objects.all()
            camaras_ordenadas = order_num_coments(camaras)
            longitud_comentarios = len(Comentario.objects.all())
            if camaras:
                img = random.choice(camaras)
            else:
                img = None
            # Me crep un objeto paginador
            paginador = Paginator(camaras_ordenadas, 3)
            # Obtengo la pagina
            numero_pagina = request.GET.get("page")
            try:
                numero_camras = paginador.page(numero_pagina)
            except PageNotAnInteger:
                numero_camras = paginador.page(1)
            except EmptyPage:
                numero_camras = paginador.page(paginador.num_pages)
            c = (Configuracion.objects.get
                 (sessionid=request.session.get("session_id")))
            return render(request, "TeVeO/camaras.html",
                          {"camaras": numero_camras,
                           "longitud_comentarios": longitud_comentarios,
                           "imagen_random": img,
                           "configuracion": c})
        except Configuracion.DoesNotExist:
            return redirect("main")


def camara(request, id):
    """

    Esta view se encarga de devolver la pagina html
    relacionada al id , es decir, una pagina
    en la que se ve la informacion de una camara

    :param request:
    :param id: identificador de la camara
    :return: Pagina html con el contenido de la camara y comentarios
    """

    if request.method == "GET":
        # Longitud de las camaras
        longitud_camaras = len(Camara.objects.all())
        # Longitud de los comentarios
        longitud_comentarios = len(Comentario.objects.all())
        try:
            # Obtengo la camara con id=id
            camara = Camara.objects.get(id=id)
            # Obtenemos los comentarios ordenados,
            # odenados de mas reciente a mas antiguo,
            # el -fecha lo que dice es que me lo
            # devuelva el valor mas alto al mas bajo.
            comentarios = (Comentario.objects.filter
                           (camera=camara).order_by("-fecha"))
            c = (Configuracion.objects.get
                 (sessionid=request.session.get("session_id")))
            return render(request, "TeVeO/camara.html",
                          {"camara": camara,
                           "comentarios": comentarios,
                           "longitud_camaras": longitud_camaras,
                           "longitud_comentarios": longitud_comentarios,
                           "configuracion": c})
        except Camara.DoesNotExist:
            return HttpResponse(f"No existe la ninguna camara "
                                f"con el identificador {id}", status=404)
        except Configuracion.DoesNotExist:
            return redirect("main")

    elif request.method == "POST":
        camara = Camara.objects.get(id=id)
        like = request.POST.get("like")
        if like:
            Like = LikeCamara(camara=camara)
            Like.save()
        return redirect("camara", id=id)


def poner_comentario(request):
    """
    Vista encargar de si se recibe un GET,
    se envia la pagina para poner un comentario.
    Si se recibe un POST, se crea un numero comentario,
    en el que se guarda el comentario, el
    autor, fecha y la imagen en ese momento

    :param request:
    :return: Pagina con informacionde la camara y
    un formulario para poenr un comentario de esa camara
    """
    if request.method == "GET":
        # Obtengo el ID
        camera_id = request.GET.get("id")
        try:
            # Obtengo la camara con ID = id
            camara = Camara.objects.get(id=camera_id)
            # Fecha del momento actual
            fecha = timezone.now()
            confi = (Configuracion.objects.get
                     (sessionid=request.session.get("session_id")))
            return render(request,
                          "TeVeO/comentario.html",
                          {"camara": camara,
                           "fecha": fecha,
                           "comentarios": Comentario.objects.all(),
                           "camaras": Camara.objects.all(),
                           "configuracion": confi})

        except Camara.DoesNotExist:
            return HttpResponse(f"La cámara con el ID "
                                f"{camera_id} no existe", status=404)
        except Configuracion.DoesNotExist:
            return redirect("main")

    elif request.method == "POST":
        # Obtengo el id
        camera_id = request.POST.get("camera_id")
        # Obtengo el comentario
        comentario_texto = request.POST["comentario"]
        try:
            # Obtengo la camara con ese id
            camera = Camara.objects.get(id=camera_id)
            # Obtengo el nombre con el que se ha identificado
            autor = Configuracion.objects.get(
                sessionid=request.session["session_id"]).nombre
            # Fecha del momento actual
            fecha = timezone.now()
            # Descargo la imagen del momento en que se pone el comentario
            image_bytes = download_image(camera_id)
            if image_bytes:
                # Convierto lo bytes en una cadena en base64
                image_base64 = base64.b64encode(image_bytes).decode("utf-8")
                c = Comentario(camera=camera,
                               autor=autor,
                               fecha=fecha,
                               comentario=comentario_texto,
                               imagen=image_base64)
                c.save()
            else:
                c = Comentario(camera=camera, autor=autor,
                               fecha=fecha, comentario=comentario_texto)
                c.save()
            return redirect("camara", id=camera_id)
        except Camara.DoesNotExist:
            return HttpResponse(f"La cámara con el ID {camera_id} no existe",
                                status=404)


def terminar_session(request):
    """
    Lo quer hace esta funcion es obtener la cookie de sesion
    del navegador y resetea los valores de la configuracion.
    Los poner por el valor con defecto al momento en el que entro
    """
    id_session = request.session.get("session_id")

    configuracion = Configuracion.objects.get(sessionid=id_session)
    configuracion.nombre = "Anonimo"
    configuracion.size = "medium"
    configuracion.tipo = "serif"
    configuracion.save()
    return redirect("main")


def cambiar_configuracion(request, confi):
    """

    Funcion que cambia los valores de la configuracion
    de tamano y tipo de letra con la de la configruacion que ha
    obtenido.
    """

    id_actual = request.session.get("session_id")

    confi_actual = Configuracion.objects.get(sessionid=id_actual)
    confi_actual.size = confi.size
    confi_actual.tipo = confi.tipo
    confi_actual.save()


def autorizar(request):

    """
    Funcion que autoriza , obtenine la cookie de sesion
    la cambia la configuracion y redirecciona a la pagina de la
    configuracion
    """
    session_id = request.GET.get('id')
    if session_id:
        try:
            # Intenta obtener la configuración
            # de la sesión proporcionada
            configuracion_nueva = (
                Configuracion.objects.get(sessionid=session_id))
            cambiar_configuracion(request, configuracion_nueva)
        except Configuracion.DoesNotExist:
            # Si no existe tal configuración,
            # redirige al usuario a una página de error
            return redirect("configuracion")

        # Establece el ID de la sesión del usuario actual
        # al ID de la sesión proporcionado
        request.session["session_id"] = session_id

        # Redirige al usuario a la página de configuración
        return redirect("configuracion")
    else:
        # Si no se proporcionó un ID de sesión,
        # redirige al usuario a una página de error
        return HttpResponse("Ha ocurrido un error", status=404)


def configuracion(request):
    """
    Vista encargada de devovlver una pagina,
    en la que tenemos 3 formularios, para poner
    el nombre del comentador, tipo de letra y tamaño
    """

    if request.method == "GET":
        # Obtenemos el unico objeto que tenemos de configuracion
        session = request.session.get("session_id")
        try:
            # obtengo la configuracion con esa cookie de sesion
            configuracion = Configuracion.objects.get(sessionid=session)

            # obtengo la penultima sesion
            last_sesion = None
            try:
                last_sesion = Configuracion.objects.all()[1]
            except IndexError:
                last_sesion = Configuracion.objects.all()[0]

            # obtengo su el id de la cookie
            sesion_to_change = last_sesion.sessionid

            # enlace para autorizar
            url_autorizar = reverse("autorizar") + f"?id={sesion_to_change}"

            # Obtenemos las listas de tamaño y tipo de letra
            size = ['small', 'x-small', 'small', 'medium',
                    'large', 'x-large', 'xx-large', 'xxx-large']
            tipo = ['serif', 'sans-serif', 'monospace', 'cursive',
                    'fantasy', 'system-ui', 'ui-serif',
                    'ui-sans-serif', 'ui-monospace',
                    'ui-rounded', 'emoji', 'math', 'fangsong']
            return render(request, "TeVeO/configuracion.html",
                          {"size": size,
                           "tipo": tipo,
                           "configuracion": configuracion,
                           "camaras": Camara.objects.all(),
                           "comentarios": Comentario.objects.all(),
                           "url_autorizar": url_autorizar})
        except Configuracion.DoesNotExist:
            return HttpResponse(f"Ha ocurrido un error", status=404)

    elif request.method == "POST":
        confi = (Configuracion.objects.get
                 (sessionid=request.session.get("session_id")))
        # Si en la POST esta nombre
        if "nombre" in request.POST:
            # Actualizo el campo nombre
            nombre = request.POST.get("nombre")
            confi.nombre = nombre
        # Si en el POST esta size
        if "size" in request.POST:
            # Actualizo el campo size
            size = request.POST.get("size")
            confi.size = size
        # Si en el POST esta tipo
        if "tipo" in request.POST:
            # Actualizo el campo tipo
            tipo = request.POST.get("tipo")
            confi.tipo = tipo
        # Guardo la modificacion
        confi.save()
        # Redireciono a la pagina de la configuracion
        return redirect("configuracion")


def ayuda(request):
    """
    Vista que devuelve una pagina que indica el funcionamiento y documentacion
    """
    try:
        confi = (Configuracion.objects.get
                 (sessionid=request.session.get("session_id")))
        return render(request, "TeVeO/ayuda.html",
                      {"camaras": Camara.objects.all(),
                       "comentarios": Comentario.objects.all(),
                       "configuracion": confi})
    except Configuracion.DoesNotExist:
        return redirect("main")


headers = {
    'User-Agent': 'Mozilla/5.0 '
                  '(Windows NT 10.0; Win64; x64; rv:100.0) '
                  'Gecko/20100101 Firefox/100.0'
}


def download_image(id):
    """Download image and return it as bytes"""
    camara = None
    try:
        camara = Camara.objects.get(id=id)
        url = camara.source
        request = urllib.request.Request(url=url, headers=headers)
    except Camara.DoesNotExist:
        print("No existe id para esa camara")
    try:
        with urllib.request.urlopen(request) as response:
            image = response.read()
    except urllib.error.URLError as e:
        return None
    return image


def image():
    """Return the image as bytes"""
    image = download_image(id)
    if image is None:
        return HttpResponse('Problems', status=500)
    else:
        return HttpResponse(image, content_type="image/jpeg")


def image_embedded(request):
    """Return HTML text, with a IMG element embedding the image"""
    id = request.GET.get("id")
    image = download_image(id)
    if image is None:
        return HttpResponse('Problems', status=500)
    else:
        image_base64 = base64.b64encode(image).decode('utf-8')
        html = f'<img src="data:image/jpeg;base64,{image_base64}">'
        return HttpResponse(html, content_type="text/html")


def camaradyn(request, id):
    """
    Vista encargada de devolve la pagina dinamica de la camara con id=id
    :param request:
    :param id: identificador de la camara
    """
    if request.method == "GET":
        # Obtengo la longitud de las camaras
        longitud_camaras = len(Camara.objects.all())
        # Obtengo la longitud de los comentarios
        longitud_comentarios = len(Comentario.objects.all())
        try:
            # Obtengo la camara con ese id
            camara = Camara.objects.get(id=id)
            # Obtengo los comentarios ordenados por fecha mas actual
            comentarios = Comentario.objects.filter(
                camera=camara).order_by("-fecha")
            confi = (Configuracion.objects.get
                     (sessionid=request.session.get("session_id")))
            return render(request,
                          "TeVeO/camaradyn.html",
                          {"camara": camara,
                           "longitud_camaras": longitud_camaras,
                           "longitud_comentarios": longitud_comentarios,
                           "comentarios": comentarios,
                           "configuracion": confi})
        except Camara.DoesNotExist:
            return HttpResponse(f"No existe la ninguna camara "
                                f"con el identificador {id}", status=404)
        except Configuracion.DoesNotExist:
            return redirect("main")

    elif request.method == "POST":
        # obtengo la camara
        camara = Camara.objects.get(id=id)
        # veo si le ha dado al boton, y obtengo el valor True
        like = request.POST.get("like")
        if like:
            # Me creo un obejto like y lo guardo
            Like = LikeCamara(camara=camara)
            Like.save()
        return redirect("camara-dyn", id=id)


def devolver_comentario(request):
    """
    Son los comentarios que delvolvemos
    de forma dinamica para la caamara dinamica
    """
    id = request.GET.get("id")
    try:

        camara = Camara.objects.get(id=id)
        comentarios = \
            (Comentario.objects.filter(camera=camara).order_by("-fecha"))
        response_html = ""
        if comentarios:
            for comentario in comentarios:
                response_html += f"""
                            <div id="comment-box">
                                <div class="text-container">
                                    <p>Autor: {comentario.autor}</p>
                                    <p>Fecha: {comentario.fecha}</p>
                                    <p>Comentario: {comentario.comentario}</p>
                                </div>
                                '<img src="data:image/jpeg;base64,
                                {comentario.imagen}" alt="Imagen">'
                            </div>
                        """
        else:
            response_html = "<p>No hay comentarios disponibles.</p>"
        return HttpResponse(response_html, content_type="text/html")
    except Camara.DoesNotExist:
        return HttpResponse("No existe la camara en la base de datos",
                            status=404)


def css(request):
    """
    Se encargar de devolver el css minimo pedido,
    es decir, tipo de letra y tamaño y el tamaño de imagen
    """
    session_id = request.session.get("session_id")
    confi = Configuracion.objects.get(sessionid=session_id)

    return render(request, "TeVeO/sizeImagen.css",
                  {"size": confi.size, "tipo": confi.tipo},
                  content_type="text/css")


def boot(request):
    """
    Se encarga de devolver el contenido css de bootstrapp
    """

    return render(request, "TeVeO/bootstrap.css", {}, content_type="text/css")
