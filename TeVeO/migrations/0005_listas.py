# Generated by Django 5.0.3 on 2024-05-01 18:07

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('TeVeO', '0004_alter_comentario_imagen'),
    ]

    operations = [
        migrations.CreateModel(
            name='Listas',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('lista_1', models.BooleanField(default=False)),
            ],
        ),
    ]
