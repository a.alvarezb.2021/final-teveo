from django.urls import path
from . import views


urlpatterns = [
    #indica al dispacher que tiene que ir a la vista main
    path('', views.main, name='main'),
    #indica al dispacher que tiene que ir a la vista de configuracion
    path('configuracion/', views.configuracion, name='configuracion'),
    #indica al dispacher que tiene que ir a la vista autorizar
    path('autorizar', views.autorizar, name='autorizar'),
    #indica al dispacher que tiene que ir a la vista terminar
    path('terminar/', views.terminar_session, name='terminar'),
    #indica al dispacher que tiene que ir a la vista ayuda
    path('ayuda/', views.ayuda, name='ayuda'),
    #indica al dispacher que tiene que ir a la vista jsoncamara
    path('json', views.jsonCamara, name="jsonCamara"),
    #indica al dispacher que tiene que ir a la vista camaras
    path('camaras/', views.camaras, name="camaras"),
    #indica al dispacher que tiene que ir a la vista para poner comentarios
    path('comentario', views.poner_comentario, name="comentario"),
    #indica al dispacher que tiene que ir a la vista comentarios
    path('comentarios', views.devolver_comentario, name="comentarios"),
    #indica al dispacher que tiene que ir a la vista de la camara dinamica
    path('<str:id>-dyn/', views.camaradyn, name="camara-dyn"),
    #indica al dispacher que tiene que ir a la vista main de la imagen
    path('image', views.image, name="image"),
    #indica al dispacher que tiene que ir a la vista de la imagen embebida
    path('image_embedded/', views.image_embedded, name="image_embedded"),
    #indica al dispacher que tiene que ir a la vista de la camara
    path('<str:id>/', views.camara, name="camara"),
    #indica al dispacher que tiene que ir a la vista css
    path("css", views.css, name="style"),
    #indica al dispacher que tiene que ir a la vista bootstrapp
    path("bootstrapp", views.boot, name="bootstrapp"),

]