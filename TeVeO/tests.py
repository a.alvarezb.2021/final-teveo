from django.test import TestCase
from django.test import Client
from django.urls import reverse
from django.apps import apps



class MainOTestCase(TestCase):

    def setUp(self):
        self.client = Client()

    def test_main_view(self):
        response = self.client.get(reverse('main'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeO/main.html')


class CamarasTestCase(TestCase):

    def test_camaras_view(self):
        #en caso de recibir un GET
        r = self.client.get(reverse('main'))
        response = self.client.get(reverse('camaras'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeO/camaras.html')
        #En caso de recibir un POST redirecciona
        p = self.client.post(reverse('camaras'))
        self.assertEqual(p.status_code, 302)


class CamaraTestCase(TestCase):

    def setUp(self):
        self.Camara = apps.get_model('TeVeO', 'Camara')

        self.camara = self.Camara.objects.create(
            id=1,
            source="https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg",
            lugar="TV24-PTE. MEDITERRÁNEO",
            latitud="-4.424923000000033",
            longitud="36.73751199999788"
        )

    def test_camaras_view(self):
        r = self.client.get(reverse('main'))
        # Obtener la URL para la cámara con su ID
        url = reverse('camara', args=[self.camara.id])

        # Simular una solicitud HTTP hacia la vista de la cámara
        response = self.client.get(url)

        # Verificar si la respuesta es exitosa (código de estado 200)
        self.assertEqual(response.status_code, 200)

        # Verificar que la plantilla correcta se está utilizando
        self.assertTemplateUsed(response, 'TeVeO/camara.html')

        url_m = reverse('camara', args=["423324"])

        # Simular una solicitud HTTP hacia la vista de la cámara
        response_m = self.client.get(url_m)
        self.assertEqual(response_m.status_code, 404)

        p = self.client.post(reverse('camara', args=[self.camara.id]))
        self.assertEqual(p.status_code, 302)

    def tearDown(self):
        # Limpiar los objetos creados después de cada test
        self.camara.delete()

class CamaraDynTestCase(TestCase):

    def setUp(self):
        self.Camara = apps.get_model('TeVeO', 'Camara')

        self.camara = self.Camara.objects.create(
            id=1,
            source="https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg",
            lugar="TV24-PTE. MEDITERRÁNEO",
            latitud="-4.424923000000033",
            longitud="36.73751199999788",
        )

    def test_camarasdyn_view(self):
        r = self.client.get(reverse('main'))
        # Obtener la URL para la cámara con su ID
        url = reverse('camara-dyn', args=[self.camara.id])

        # Simular una solicitud HTTP hacia la vista de la cámara
        response = self.client.get(url)

        # Verificar si la respuesta es exitosa (código de estado 200)
        self.assertEqual(response.status_code, 200)

        # Verificar que la plantilla correcta se está utilizando
        self.assertTemplateUsed(response, 'TeVeO/camaradyn.html')

        url_m = reverse('camara-dyn', args=["423324"])

        # Simular una solicitud HTTP hacia la vista de la cámara
        response_m = self.client.get(url_m)
        self.assertEqual(response_m.status_code, 404)

        p = self.client.post(reverse('camara-dyn', args=[self.camara.id]))
        self.assertEqual(p.status_code, 302)

    def tearDown(self):
        # Limpiar los objetos creados después de cada test
        self.camara.delete()


class ComentarioTestCase(TestCase):
    def setUp(self):
        self.Camara = apps.get_model('TeVeO', 'Camara')

        self.camara = self.Camara.objects.create(
            id=1,
            source="https://ctraficomovilidad.malaga.eu/recursos/movilidad/camaras_trafico/TV-24.jpg",
            lugar="TV24-PTE. MEDITERRÁNEO",
            latitud="-4.424923000000033",
            longitud="36.73751199999788",
        )

        self.Comentario = apps.get_model('TeVeO', 'Comentario')

        self.comentario = self.Comentario.objects.create(camera=self.camara, autor="anonimo", fecha="22/4/2024", comentario="fuhiffequerfuhirfuehiuerfw", imagen="")

    def test_comentario_view(self):
        r = self.client.get(reverse('main'))

        url = reverse('comentario') + f"?id={self.camara.id}"

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeO/comentario.html')

        url_m = reverse('comentario') + f"?id={23423423}"

        response_m = self.client.get(url_m)

        self.assertEqual(response_m.status_code, 404)

    def test_devolver_comentario(self):
        url = reverse('comentarios') + f"?id={self.camara.id}"

        response = self.client.get(url)

        self.assertEqual(response.status_code, 200)




class ConfiguracionTestCase(TestCase):

    def test_configuracion_view(self):
        r = self.client.get(reverse('main'))
        response = self.client.get(reverse('configuracion'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeO/configuracion.html')

        p = self.client.post(reverse('configuracion'))
        self.assertEqual(p.status_code, 302)

    def test_terminar_view(self):
        r = self.client.get(reverse('main'))
        response_ = self.client.get(reverse('terminar'))
        self.assertEqual(response_.status_code, 302)


class AyudaTestCase(TestCase):

    def test_ayuda_view(self):
        r = self.client.get(reverse('main'))
        response = self.client.get(reverse('ayuda'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeO/ayuda.html')


class SizeImageTestCase(TestCase):

    def test_size_image_view(self):
        r = self.client.get(reverse('main'))
        response = self.client.get(reverse('style'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeO/sizeImagen.css')


class BootStrapTestCase(TestCase):
    def test_bootstrap_view(self):

        r = self.client.get(reverse('main'))
        response = self.client.get(reverse('bootstrapp'))
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'TeVeO/bootstrap.css')


