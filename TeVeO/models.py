from django.db import models

# Create your models here.
class Configuracion(models.Model):
    """
    Base de datos usada para guardar informacion acerta del nombre del visitante, tamaño y tipo de letra
    """

    sessionid = models.CharField(max_length=100, primary_key=True, default='')
    nombre = models.CharField(default="Anonimo", max_length=100)
    size = models.CharField(max_length=100)
    tipo = models.CharField(max_length=100)


class Camara(models.Model):
    """
    Base de datos encargada de guardar la informacion sobre una camara
    """

    id = models.CharField(max_length=100, primary_key=True)
    source = models.URLField()
    lugar = models.CharField(max_length=200)
    longitud = models.CharField(max_length=200, default="")
    latitud = models.CharField(max_length=200, default="")


class Comentario(models.Model):
    """
    Base de datos encargada de guardar la informacion sobre un comentario, asociado a una camara
    """
    camera = models.ForeignKey(Camara, on_delete=models.CASCADE)
    autor = models.CharField(default="Anonimo",max_length=100)
    fecha = models.DateTimeField(auto_now_add=True)
    comentario = models.TextField()
    imagen = models.TextField(null=True, blank=True)


class Listas(models.Model):
    """
    Base de datos encargada de guardar el valor de si se ha descargado o no una lista de camaras
    """
    lista_1 = models.BooleanField(default=False)
    lista_2 = models.BooleanField(default=False)
    lista_3 = models.BooleanField(default=False)
    lista_4 = models.BooleanField(default=False)


class LikeCamara(models.Model):
    camara = models.ForeignKey(Camara, on_delete=models.CASCADE)


